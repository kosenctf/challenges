<?php
require("base85.class.php");

if (isset($_POST['name']) && isset($_POST['desc'])) {
    if (is_uploaded_file($_FILES['image']['tmp_name'])) {
	// Check errors
	if ($_FILES['image']['error'] > 0) {
	    header("Location: /post.php");
	    exit();
	}
	// Check the file type
	$tmp_name = $_FILES['image']['tmp_name'];
	$finfo = finfo_open(FILEINFO_MIME_TYPE);
	$mimetype = finfo_file($finfo, $tmp_name);
	if ($mimetype != "image/jpeg") {
	    header("Location: /post.php");
	    exit();
	}
	// Save
	$binary = file_get_contents($_FILES['image']['tmp_name']);
	$image = base85::encode($binary);
	$tag = hash("sha256", $image);
	$pdo = new PDO('mysql:host=localhost;dbname=uploader;charset=utf8', 'uploader', 'password',
		       array(PDO::ATTR_EMULATE_PREPARES => false));
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	try {
	    // Create a new record
	    $stmt = $pdo->prepare('INSERT INTO images(tag, username, description, image) VALUES(?, ?, ?, ?);');
	    $stmt->execute([$tag, $_POST['name'], $_POST['desc'], $image]);
	} catch(Exception $e) {
	    //
	}
    } else {
	header("Location: /post.php");
	exit();
    }
} 
?>
<!DOCTYPE html>
<html>
    <head>
	<meta charset="UTF-8">
	<title>New Post - Image Uploader</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" crossorigin="anonymous"></script>
    </head>
    <body class="bg-dark">
	<div class="container">
	    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<a class="navbar-brand" href="/">Uploader</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
		    <ul class="navbar-nav ml-auto">
			<li class="nav-item">
			    <a class="nav-link" href="/">Home</a>
			</li>
			<li class="nav-item active">
			    <a class="nav-link" href="/post.php">Post</a>
			</li>
			<li class="nav-item">
			    <a class="nav-link disabled" href="/admin.html">Control</a>
			</li>
		    </ul>
		</div>
	    </nav>
	    <?php if (isset($tag)) { ?>
		<div class="alert alert-success" role="alert">
		    <strong>Uploaded!</strong> Copy the following URL to share the image.<br>
		    <a href="http://web.kosenctf.com:8400/?tag=<?= $tag ?>">http://web.kosenctf.com:8400/?tag=<?= $tag ?></a>
		</div>
	    <?php } ?>
	    <div>
		<form class="text-light" enctype="multipart/form-data" action="/post.php" method="POST">
		    <div class="form-group">
			<label for="name">Username</label>
			<input type="text" class="form-control" id="name" placeholder="Your name" name="name">
		    </div>
		    <div class="form-group">
			<label for="desc">Description</label>
			<input type="text" class="form-control" id="desc" placeholder="Discription of the image" name="desc">
		    </div>
		    <div class="form-group">
			<label for="image">Image</label>
			<input type="file" class="form-control-file" id="image" aria-describedby="imageHelp" name="image">
			<small id="imageHelp" class="form-text text-muted">
			    JPEG images smaller than 1MB are allowed.
			</small>
		    </div>
		    <button type="submit" class="btn btn-primary">Post!</button>
		</form>
	    </div>
	</div>
    </body>
</html>
