# Inter Kosen CTF 問題リスト

## 問題一覧
| 問題名          | ジャンル  | 配点|完成| Dockerize | ポート| 問題文 | 配布ファイル |
|-----------------|-----------|-----|----|-----------|-------|--------|--------------|
| Gimme Chocolate | Web       | 100 | o  | o         | 8100  | o      | -            |
| Secure Session  | Web       | 200 | o  | o         | 8200  | o      | -            |
| Login           | Web       | 250 | o  | o         | 8300  | x      | -            |
| Image Uploader  | Web       | 350 | o  | o         | 8400  | o      | o            |
| Login Reloaded  | Web       |  50 | o  | o         | 8500  | o      | o            |
| flag generator  | Rev       | 100 | o  | -         | -     | o      | o            |
| flag checker    | Rev       | 200 | o  | -         | -     | o      | o            |
| Rolling Triangle| Rev       | 300 | o  | -         | -     | o      | o            |
| strengthened    | Crypto    | 100 | o  | -         | -     | o      | o            |
| Oh my Hash      | Crypto    | 150 | o  | o         | 10200 | o      | o            |
| slot machine    | Crypto    | 300 | x  | o         | 10300 | o      | o            |
| double check    | Pwn       | 100 | o  | o         | 9100  | o      | o            |
| introduction    | Pwn       | 200 | o  | o         | 9200  | o      | o            |
| ziplist         | Pwn       | 350 | o  | o         | 9300  | o      | o            |
| sandbox         | Pwn       | 300 | o  | o         | 9400  | o      | o            |
| lights out      | Cheat     | 100 | o  | -         | -     | o      | o            |
| spaceship       | Cheat     | 150 | o  | o         | 11200 | o      | x            |
| anti cheat      | Cheat     | 250 | o  | o         | 11300 | o      | -            |
| attack log      | Forensics |  50 | o  | -         | -     | o      | o            |
| conversation    | Forensics | 200 | o  | -         | -     | o      | o            |
| matroska        | Forensics | 250 | o  | -         | -     | o      | o            |

| 問題名          | 作問者         | テスター                 |Flag                                                                               |
|-----------------|----------------|--------------------------|-----------------------------------------------------------------------------------|
| Gimme Chocolate | theoldmoon0602 | ptr-yudai                |`KOSENCTF{CIO_CHOCOLATEx2_CHOx3_IIYONE}`                                           |
| Secure Session  | ptr-yudai      | theoldmoon0602           |`KOSENCTF{Th3_p01nt_1s_N0t_h0w_s3cur3_1t_1s_But_h0w_t0_us3_1t}`                    |
| Login           | theoldmoon0602 | ptr-yudai                |`KOSENCTF{I_DONT_HAVE_ANY_APTITUDES_FOR_MAKING_WEB_CHALLENGE_SORRY}`               |
| Image Uploader  | ptr-yudai      | x                        |`KOSENCTF{awkward_SQLi_2_bypass_WAF_module}`                                       |
| Login Reloaded  | theoldmoon0602 | x                        |`KOSENCTF{FORGIVE_ME_FORGIVE_ME_FORGIVE_ME}`                     |
| flag generator  | theoldmoon0602 | ptr-yudai                |`KOSENCTF{IS_THIS_REALLY_A_REVERSING?}`                                            |
| flag checker    | theoldmoon0602 | ptr-yudai                |`KOSENCTF{TOO_EASY_TO_DECODE_THIS}`                                                |
| Rolling Triangle| theoldmoon0602 | x                        |`KOSENCTF{DO_YOU_KNOW_OF_METAL_MOMOKO}`                                            |
| strengthened    | theoldmoon0602 | ptr-yudai/yoshiking      |`KOSENCTF{THIS_ATTACK_DOESNT_WORK_WELL_PRACTICALLY}`                               |
| Oh my Hash      | theoldmoon0602 | ptr-yudai/yoshiking      |`KOSENCTF{PADDING_MAKES_THIS_MORE_VULNERABLE}`                                     |
| slot machine    | ptr-yudai      | theoldmoon0602           |`KOSENCTF{wr0ng_us4g3_0f_3ll1pt1c_curv3_f0r_s1gn4tur3}`                            |
| double check    | ptr-yudai      | theoldmoon0602/yoshiking |`KOSENCTF{s1mpl3-st4ck0v3rfl0w!}`                                                  |
| introduction    | ptr-yudai      | theoldmoon0602/yoshiking |`KOSENCTF{lIbc-bAsE&ESp_lEAk+rET2lIbc_ThrOugh_FSB}`                                |
| ziplist         | ptr-yudai      | x                        |`KOSENCTF{H3ap0v3rfl0w+G0T0v3rwrit3+KillSSP+Buff3r0v3rfl0w}`                       |
| sandbox         | ptr-yudai      | x                        |`KOSENCTF{4lw4ys_w0nd3r3d_why_p3opl3_b4r3ly_us3_th3ir_str0ng3st_pr0t3cti0n_first}` |
| lights out      | ptr-yudai      | theoldmoon0602/yoshiking |`KOSENCTF{st4tic4lly_d3obfusc4t3_OR_dyn4mic4lly_ch34t}`                            |
| spaceship       | ptr-yudai      | thrust2799               |`KOSENCTF{0nlin3_ch34t_by_f4k3_p4ck3t}`                                            |
| anti cheat      | ptr-yudai      | yoshiking                |`KOSENCTF{bASIc_buT_STrOng_AnTI_chEAT}`                                            |
| attack log      | ptr-yudai      | theoldmoon0602           |`KOSENCTF{bRut3F0rc3W0rk3D}`                                                       |
| conversation    | ptr-yudai      | theoldmoon0602           |`KOSENCTF{7h3_4r7_0f_4ndr01d_f0r3n51c5}`                                           |
| matroska        | ptr-yudai      | theoldmoon0602           |`KOSENCTF{Use_After_Free_Arbitrary_Code_Execution}`                                |
