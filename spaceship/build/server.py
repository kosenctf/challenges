import sys
import os
import struct

flag = "KOSENCTF{0nlin3_ch34t_by_f4k3_p4ck3t}"

if __name__ == '__main__':
    # Unpack the packet
    try:
        #sys.stdout = os.fdopen(sys.stdout.fileno(), 'w', 0)
        magic = sys.stdin.read(4)
        score = struct.unpack('<I', bytes(sys.stdin.read(4), 'UTF-8'))[0]
        length = ord(sys.stdin.read(1))
        name = sys.stdin.read(length)
    except Exception as e:
        #print(e)
        exit(1)
    # Magic number
    if magic != 'SPST':
        sys.stdout.write('{"response":"Invalid header."}')
        exit(1)
    # Length check
    if length != len(name):
        sys.stdout.write('{"response":"Invalid header."}')
        exit(1)
    # Score check
    if score < 10000:
        sys.stdout.write('{"response":"Try harder."}')
    else:
        sys.stdout.write('{"response":"' + flag + '"}')
