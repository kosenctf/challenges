#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

char auth = 1;
char password[32];

void handler(int sig)
{
  if (sig == SIGALRM) {
    puts("\nTimeout.");
    exit(1);
  }
}

void init(void)
{
  setvbuf(stdout, NULL, _IONBF, 0);
  signal(SIGALRM, handler);
  alarm(5);
}

void sw(char flag)
{
  auth = flag;
}

char readfile(char* path, char* buf)
{
  FILE *fp;
  if (auth == 1) {
    fp = fopen(path, "r");
    fread(buf, 1, 31, fp);
    fclose(fp);
    buf[31] = 0;
    auth = 0;
  } else {
    return 1;
  }
  return 0;
}

int main(void)
{
  FILE *fp;
  char input[32];

  init();
  
  if (readfile("password", password)) {
    puts("The file is locked.");
    return 1;
  }
  
  printf("Password: ");
  scanf("%s", input);

  if (strncmp(password, input, 31) == 0) {
    sw(1);
  } else {
    sw(0);
  }
  
  if (strncmp(password, input, 31) == 0) {
    if (readfile("flag", password)) {
      puts("The file is locked.");
    } else {
      printf("%s\n", password);
    }
  } else {
    puts("Invalid password.");
  }

  memset(password, '\x00', 32);
  return 0;
}
