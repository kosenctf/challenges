import struct

k = 0xDEC0C0DE
x = "\x9f\xc9\xd7\xc2\x0a\x46\x44\x59\x84\xc5\xce\xc1\x3f\x4f\x5f\x4e\xbe\xd4\xde\xdd\x39\x4b\x4a\x4c\xa6\xcf\xd1\xd1\x29\x55\x4a\x4e\xa3\xc3\xc3\xdd"

h = 0
ms = []
for i in range(len(x) / 4 - 1, -1, -1):
    y = struct.unpack("<I", x[i*4:(i+1)*4])[0]
    m = k ^ y ^ h
    ms.append(struct.pack("<I", m))
    h = m ^ k ^ h
    k = ((k << 8) | (k >> 24)) & 0xFFFFFFFF;
print("".join(reversed(ms)))


