#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main() {
        FILE *rp;
        FILE *wp;

        srand(time(NULL));
        if ((rp = fopen("flag.txt", "r")) == NULL) {
                perror("fopen");
                return 1;
        }
        if ((wp = fopen("encrypted.txt", "w")) == NULL) {
                perror("fopen");
                return 1;
        }

        int c;
        while ((c = fgetc(rp)) != EOF) {
                fputc((c ^ rand()) & 0xff, wp);
        }

        fclose(rp);
        fclose(wp);

        return 0;
}
