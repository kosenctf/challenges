<?php
require("base85.class.php");

if (isset($_GET['tag'])) {
    $search = true;
    $pdo = new PDO('mysql:host=localhost;dbname=uploader;charset=utf8', 'uploader', 'password',
		   array(PDO::ATTR_EMULATE_PREPARES => false));
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
    try {
	// Get the image
	$stmt = $pdo->prepare('SELECT * FROM images WHERE tag=?');
	$stmt->execute([$_GET['tag']]);
	$result = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($result) {
	    $name = $result['username'];
	    $desc = $result['description'];
	    $image = base64_encode(base85::decode($result['image']));
        } else {
	    $image = NULL;
	}
    } catch (Exception $e) {
        //
	$image = NULL;
    }
}

if (isset($_POST['search'])) {
    if (is_uploaded_file($_FILES['image']['tmp_name'])) {
	// Check errors
	if ($_FILES['image']['error'] > 0) {
	    header("Location: /");
	    exit();
	}
	// Check the file type
	$tmp_name = $_FILES['image']['tmp_name'];
	$finfo = finfo_open(FILEINFO_MIME_TYPE);
	$mimetype = finfo_file($finfo, $tmp_name);
	if ($mimetype != "image/jpeg") {
	    header("Location: /");
	    exit();
	}
	// Search
	$search = true;
	$pdo = new PDO('mysql:host=localhost;dbname=uploader;charset=utf8', 'uploader', 'password',
		       array(PDO::ATTR_EMULATE_PREPARES => false));
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	try {
	    $binary = file_get_contents($_FILES['image']['tmp_name']);
	    $s_image = base85::encode($binary);
	    // Get the image
	    $row = $pdo->query("SELECT * FROM images WHERE image='{$s_image}'")->fetch(PDO::FETCH_ASSOC);
	    if ($row) {
		$name = $row['username'];
		$desc = $row['description'];
		$image = base64_encode(base85::decode($row['image']));
	    } else {
		$image = NULL;
	    }
	} catch (Exception $e) {
	    //
	    $image = NULL;
	}
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
	<meta charset="UTF-8">
	<title>Image Uploader</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" crossorigin="anonymous"></script>
    </head>
    <body class="bg-dark">
	<div class="container">
	    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<a class="navbar-brand" href="/">Uploader</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
		    <ul class="navbar-nav ml-auto">
			<li class="nav-item active">
			    <a class="nav-link" href="/">Home</a>
			</li>
			<li class="nav-item">
			    <a class="nav-link" href="/post.php">Post</a>
			</li>
			<li class="nav-item">
			    <a class="nav-link disabled" href="/admin.html">Control</a>
			</li>
		    </ul>
		</div>
	    </nav>
	    <div class="text-light">
		<h3>Uploaded Images</h3>
		<p>Enter the image tag to show the image.</p>
		<form action="/" method="GET">
		    <label for="tag">Tag</label>
		    <div class="form-row">
			<div class="col">
			    <input type="text" class="form-control" id="tag" placeholder="Image tag" name="tag">
			</div>
			<div class="col">
			    <button type="submit" class="btn btn-primary">Go!</button>
			</div>
		    </div>
		</form>
	    </div>
	    <div class="mt-3 text-light">
		<?php if (isset($search)) { ?>
		    <?php if ($image === NULL) { ?>
			<div class="alert alert-danger" role="alert">
			    Sorry, we couldn't find any matching images.
			</div>
		    <?php } else { ?>
			<hr>
			<p>Uploaded by <?= htmlspecialchars($name) ?></p>
			<figure class="figure">
			    <img src="data:image/jpeg;base64,<?= $image ?>" class="figure-img img-fluid rounded" alt="Uploaded image">
			    <figcaption class="figure-caption"><?= htmlspecialchars($desc) ?></figcaption>
			</figure>
		    <?php } ?>
		<?php } ?>
	    </div>
	</div>
    </body>
</html>
