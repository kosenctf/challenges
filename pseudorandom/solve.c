#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>

int main() {
        struct stat st;
        FILE *rp;

        stat("encrypted.txt", &st);
        srand( st.st_mtime );

        if ((rp = fopen("encrypted.txt", "r")) == NULL) {
                perror("fopen");
                return 1;
        }

        int c;
        while ((c = fgetc(rp)) != EOF) {
                printf("%c", (c ^ rand()) & 0xff);
        }
        printf("\n");

        fclose(rp);

        return 0;
}
