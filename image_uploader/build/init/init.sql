GRANT ALL ON uploader.* TO 'uploader'@'%';
GRANT FILE ON *.* TO 'uploader'@'%';
DROP TABLE IF EXISTS `images`;
CREATE TABLE `images` (
  `tag` TINYTEXT NOT NULL,
  `username` TEXT NOT NULL,
  `description` TEXT NOT NULL,
  `image` MEDIUMTEXT NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
