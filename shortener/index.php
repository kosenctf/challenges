<?php

function redis_connect() {
  $fp = fsockopen("tcp://127.0.0.1", 6379, $errno, $errstr, 3);
  if ($fp === FALSE) {
    die("Connection error. Contact to admin.");
  }
  stream_set_timeout($fp, 1);
  return $fp;
}

function redis_escape($s) {
  $s = str_replace("\r", "\\x0d", $s);
  $s = str_replace("\n", "\\x0a", $s);
  return $s;
}

function redis_set($fp, $key, $value) {
  $value = redis_escape($value);
  fwrite($fp, "set $key \"$value\"\r\n");
  fgets($fp);
}

function redis_get($fp, $key) {
  fwrite($fp, "get $key\r\n");
  $l = (int)substr(fgets($fp), 1);
  if ($l === -1) {
    return FALSE;
  }
  return fread($fp, $l);
}


$r = redis_connect();
redis_set($r, "flag", "KOSENCTF{CTF}");

if (isset($_POST['url']) && preg_match("@^https?://@", $_POST['url'])) {
  $short = bin2hex(random_bytes(5));
  redis_set($r, $short, urlencode($_POST['url']));
  var_dump($short);
} else if (isset($_GET['q']) && !preg_match("@flag@", $_GET['q'])) {
  $long = urldecode(redis_get($r, $_GET['q']));

  var_dump($long);
  if ($long) {
    // header("Location: $long\r\n");
    fclose($r);
    exit();
  }
}

fclose($r);

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>URL Shortener</title>
  <link rel="stylesheet" type="text/css" href="https://rawcdn.githack.com/Airmime/minstyle.io/2201279f4605ef5a83bd4f1745499ec5d32a59b5/css/minstyle.io.css">
</head>
<body>
  <div class="container">
    <h1>URL Shortener</h1>
    <form method="post" action="." class="col-s-8 col-s-center">
      <div>
        <input type="text" placeholder="http://example.com/longurl" name="url" class="ms-large">
      </div>
      <div>
        <input type="submit" value="shorten this" class="ms-btn ms-rounded ms-blue">
      </div>
      
    </form>
  </div>
</body>
</html>
