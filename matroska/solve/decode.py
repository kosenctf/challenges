with open("text.dmp", "rb") as f:
    text = f.read()

flag = ""
for (i, c) in enumerate(text):
    flag += chr(ord(c) ^ 0xFF ^ (len(text) - i))

print(flag)
