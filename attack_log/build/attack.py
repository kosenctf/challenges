import requests
import string
import random

def attack(password=None):
    table = string.ascii_letters + "0123456789"
    if password is None:
        password = ''.join(random.choice(table) for i in range(16))
    r = requests.get("http://www.kosenlab.org/", auth=('kosen', password))

flag = "bRut3F0rc3W0rk3D"

for i in range(2018):
    attack()
attack(flag)
for j in range(118):
    attack()

