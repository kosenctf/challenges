<?php

$pdo = new PDO('sqlite:database.db');

$pdo->exec('create table users(username text unique not null, password text not null);');
$stmt = $pdo->prepare('insert into users(username, password) values(?, ?)');

$stmt->execute(['admin', bin2hex(random_bytes(64))]);
