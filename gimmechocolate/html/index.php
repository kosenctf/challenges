<?php

// This function evaluates a source code which is similar to brainf*ck.
// However, looking into the details, there are some differences. Be careful!
function bf($code) {
    $ARRSIZE = 10000;
    $STEPSIZE = 100000;
    $p = 0;
    $jp = 0;
    $jbuf = [];
    $mp = 0;
    $mem = array_fill(0, $ARRSIZE, 0);
    $obuf = "";
    
    for ($s = 0; $s < $STEPSIZE && $p < strlen($code); $s++) {
        $c = $code[$p];
        if ($c === '>') {
            $mp = ($mp + 1 + $ARRSIZE) % $ARRSIZE;
        } else if ($c === '<') {
            $mp = ($mp - 1 + $ARRSIZE) % $ARRSIZE;
        } else if ($c === '+') {
            $mem[$mp] = $mem[$mp] + 1;
        } else if ($c === '-') {
            $mem[$mp] = $mem[$mp] - 1;
        } else if ($c === '[') {
            $jbuf[$jp] = $p;
            $jp++;
        } else if ($c === ']') {
            if ($mem[$mp] == 0) {
                $jp--;
            } else {
                $p = $jbuf[$jp-1];
            }
        } else if ($c === '.') {
            $obuf .= chr($mem[$mp]);
        }
        $p++;
    }

    return $obuf;
}

if (isset($_GET['source'])) {
    highlight_file(__FILE__);
    die;
}

$prefix = __DIR__. DIRECTORY_SEPARATOR . 'codes';
if (!is_dir($prefix)) {
    mkdir($prefix);
}

$errs = [];
$msgs = [];
$r = "";
do {
    if (isset($_POST['save'])) {
        if (!isset($_POST['name']) || empty($_POST['name'])) {
            $errs []= 'filename required';
            break;
        }
        if (!preg_match('@^[A-Za-z0-9_]+$@', $_POST['name'])) {
            $errs []= 'invalid filename';
            break;
        }

        if (!isset($_POST['code']) || empty($_POST['code'])) {
            $errs []= 'code required';
            break;
        }

        $fp  = fopen($prefix.DIRECTORY_SEPARATOR.$_POST['name'], 'w');
        if ($fp === FALSE || fwrite($fp, $_POST['code'], 100) === FALSE) {
            $errs []= 'Unexpected error. Please contact the admin.';
            break;
        }
        fclose($fp);

        $msgs []= 'SAVED';
    }
    else if (isset($_GET['execute'])) {
        $code = file_get_contents($_GET['file']);
        if ($code === FALSE) {
            $errs []= 'Could not open the source file.';
            break;
        }
        $r = bf($code);
        if ($r === "Give Me a Chocolate!!") {
            $msgs []= include(dirname(__DIR__).DIRECTORY_SEPARATOR.'flag.php');
        }
    }
} while(false);


$files = glob($prefix.DIRECTORY_SEPARATOR.'*');
usort($files, function($a, $b){
  return filemtime($a) < filemtime($b);
});
$files = array_slice($files, 0, 10);

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Give Me a Chocolate!!</title>
    <style>
        .hoge {
            display: inline-block;
            width: 80%;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="https://rawcdn.githack.com/Airmime/minstyle.io/2201279f4605ef5a83bd4f1745499ec5d32a59b5/css/minstyle.io.css">
</head>
<body>
    <div class="container">
        <?php foreach($msgs as $msg) { ?>
            <div class="ms-alert ms-green"><?= $msg ?></div>
        <?php } ?>
        <?php foreach($errs as $err) { ?>
            <div class="ms-alert ms-red"><?= $err ?></div>
        <?php } ?>

        <?php if ($r) { ?>
            <div class="ms-alert ms-blue">
                <code><?= htmlspecialchars($r); ?></code>
            </div>
        <?php } ?>
    
        <form action="" method="POST">
            <div>
            code:
                <textarea name="code" id="" cols="30" rows="10" placeholder=""></textarea>
            </div>
            <div>
                filename: <input type="text" name="name" required/>
                <input type="submit" value="Save" name="save">
            </div>
        </form>

        <ul class="ms-list ms-boxed">
            <?php foreach($files as $f) { ?>
            <li>
                <a href="codes/<?= basename($f); ?>" class="hoge"><?= basename($f); ?></a>
                <a href="?execute&file=<?= $f ?>" class="ms-btn ms-border ms-blue  no-margin">Run</a></li>
            <?php } ?>
        </ul>

        <p>
            <a href="?source">view source</a>        
        </p>
    </div>
</body>
</html>
