invulnerable [pwn 300]
====

seccomp bypass + time-based blind info leak

## 問題
Description:
The flag is written in ```/home/pwn/flag```.
```
nc kosenctf.com 9400
```

Downloads:

- http://kosenctf.com/static/invulnerable.tar.gz

## 解法
