from pwn import *
from hashlib import md5

def strxor(a, b):
    r = ''
    for i in range(len(a)):
        x = ord(a[i]) ^ ord(b[i])
        r += chr(x)
    return r


HOST = "others.kosenctf.com"
# HOST = "localhost"

p = remote(HOST, "10300")

# register as adminXXXX... user
p.recvuntil("[l]ogin")
p.sendline("r")
p.recvuntil("Name:")
p.sendline("admin" + "X"*(32-5))

p.recvuntil("token:\n")
token = p.recvline().strip().decode("hex")
print(len(token))
p.close()

# bit flipping to modify last byte (indicates padding length)
name = strxor(chr(16)*16, chr(16*3 - 5)*16)
new_token = token[:-32] + strxor(token[-32:-16], name) + token[-16:]

# login with new_token
p = remote(HOST, "10300")

p.recvuntil("[l]ogin")
p.sendline("l")
p.recvuntil("Token:")
p.sendline(new_token.encode("hex"))
print(p.recvline())
print(p.recvline())
