#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int s;

int r() {
  s = ((s * 1103515245U) + 12345U) & 0x7fffffff;
  return s;
}

int main(int argc, char **argv) {
  s = atoi(argv[1]);

  int l = strlen(argv[2]);
  char *buf;
  if (l % 4 != 0) {
    l = l + 4 - (l % 4);
  }
  buf = malloc(sizeof(char) * l);
  memset(buf, 0, sizeof(char) * l);
  strncpy(buf, argv[2], l);

  for (int i = 0; i <  l / 4; i++) {
    int r2 = r();
    if (i == 0) {
      printf("0x%08x\n", r2);
      printf("%d\n", l / 4);
      printf("{");
    }
    int x = ((int*)buf)[i] ^ r2;
    printf("0x%08x, ", x);
  }
  printf("}\n");

  free(buf);
  return 0;
}
