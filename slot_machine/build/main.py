#!/usr/bin/env python
# coding: utf-8
import sys
import os
import hashlib
import string
import random
import re
from slot import Slot
from sign import save_token, load_token
from secret import FLAG

INITIAL_MONEY = 500

def fullmatch(regex, string):
    return re.match("(?:" + regex + r")\Z", string)

def init():
    """ Init money """
    return INITIAL_MONEY

def load():
    """ Load money """
    print("Enter the token we issued last time.")
    token = raw_input(">> ")
    money = load_token(token)
    if money is None:
        return INITIAL_MONEY
    return money

def play(money):
    """ Play a slot game """
    print("+---------------+")
    print("| MONEY: ${0:5} |".format(money))
    print("+---------------+")
    slot = Slot()
    # Bet
    while True:
        bet = raw_input("BET: ")
        try:
            bet = int(bet)
            if 0 >= bet:
                print("You have to bet at least $1.")
            elif bet > INITIAL_MONEY:
                print("You can bet at most ${}".format(INITIAL_MONEY))
            else:
                break
        except:
            pass
    money -= bet
    # Pull
    slot.pull_lever()
    # Result
    slot.show_slot()
    # Calc
    d = slot.calc_bet(bet)
    money += d
    if d == 0:
        print("You lost ${0}.".format(bet))
    elif d - bet > 0:
        print("You won ${0}.".format(d - bet))
    else:
        print("You neither lost nor won money.")
    return money

if __name__ == '__main__':
    sys.stdout = os.fdopen(sys.stdout.fileno(), 'w', 0)
    # Proof of work
    sha256 = hashlib.sha256(''.join(
        [random.choice(string.ascii_letters) for i in range(64)]
    )).hexdigest()
    print("Find an alphanumeric text such that the last 5 digits of the hexdigest of sha256(text) equals to '{}'".format(sha256[-5:]))
    text = raw_input(">> ")
    text_sha256 = hashlib.sha256(text).hexdigest()[-5:]
    if fullmatch("[a-zA-Z0-9]+", text) and text_sha256[-5:] == sha256[-5:]:
        print("Nice work.")
    else:
        print("Try again.")
        exit(0)
    # Welcome message
    print(".▄▄ · ▄▄▌        ▄▄▄▄▄    • ▌ ▄ ·.  ▄▄▄·  ▄▄·  ▄ .▄▪   ▐ ▄ ▄▄▄ .")
    print("▐█ ▀. ██•  ▪     •██      ·██ ▐███▪▐█ ▀█ ▐█ ▌▪██▪▐███ •█▌▐█▀▄.▀·")
    print("▄▀▀▀█▄██▪   ▄█▀▄  ▐█.▪    ▐█ ▌▐▌▐█·▄█▀▀█ ██ ▄▄██▀▐█▐█·▐█▐▐▌▐▀▀▪▄")
    print("▐█▄▪▐█▐█▌▐▌▐█▌.▐▌ ▐█▌·    ██ ██▌▐█▌▐█ ▪▐▌▐███▌██▌▐▀▐█▌██▐█▌▐█▄▄▌")
    print(" ▀▀▀▀ .▀▀▀  ▀█▄▀▪ ▀▀▀     ▀▀  █▪▀▀▀ ▀  ▀ ·▀▀▀ ▀▀▀ ·▀▀▀▀▀ █▪ ▀▀▀ ")

    while True:
        print("\n[P]lay / [L]oad")
        c = raw_input(">> ")
        if c == 'P' or c == 'p':
            # Play a new game
            print("Welcome!")
            money = init()
            break
        elif c == 'L' or c == 'l':
            # Load the saved status
            print("Welcome back!")
            money = load()
            break

    print("May the odds be ever in your favor.")
    while True:
        money = play(money)
        print("\n[C]ontinue / [S]ave / [E]xit")
        c = raw_input(">> ")
        if c == 'S' or c == 's':
            # Save
            token = save_token(money)
            print("Here is your token: {0}".format(token))
            break
        elif c == 'E' or c == 'e':
            # Exit
            break
        # Otherwise it continues...
        if money <= 0:
            # No money
            print("I'm afraid there's no money left.")
            break
        elif money >= 1000000:
            # Millionaire
            print("Congratulations!")
            print("We've never seen such a great triumph.")
            print(FLAG)
            break
    
    print("\nEnjoyed it? Come again!\n")
