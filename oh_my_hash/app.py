from flask import Flask, render_template, session, make_response, request, flash, redirect, url_for
from secret import SALT, ADMIN_PASSWORD, SECRET_KEY, FLAG
from myhash import myhash

g = {}
g['users'] = {"admin": myhash((SALT + ADMIN_PASSWORD).encode())}
g['tokens'] = {
    myhash((SALT + "admin").encode()).decode(): "admin"
}
app = Flask(__name__)
app.secret_key = SECRET_KEY

@app.route('/')
def index():
    if 'user' in session:
        return render_template('user.html', user=session['user'], flag=FLAG)

    token = request.cookies.get('remember_token')
    if token and token in g['tokens']:
        session['user'] = g['tokens'][token]
        resp = make_response(render_template('user.html', user=session['user'], flag=FLAG))
        resp.set_cookie('remember_token', value='', expires=0)
        return resp

    return render_template('index.html')

@app.route('/register', methods=['POST'])
def register():
    if request.method == 'POST':
        if 'user' in session:
            flash('you are already logged in', 'danger')
            return redirect(url_for('index'))

        name = request.form.get('name')
        password = request.form.get('password')

        if not name or not password:
            flash('username and password are required', 'danger')
            return redirect(url_for('index'))

        if name == "admin":
            flash('the username `admin` is forbidden', 'danger')
            return redirect(url_for('index'))

        g['users'][name] = myhash((SALT + password).encode())
        flash('successfully registered', 'success')

    return redirect(url_for('index'))

@app.route('/login', methods=['POST'])
def login():
    if request.method == 'POST':
        if 'user' in session:
            flash('you are already logged in', 'danger')
            return redirect(url_for('index'))

        name = request.form.get('name')
        password = request.form.get('password')

        if not name or not password:
            flash('username and password are required', 'danger')
            return redirect(url_for('index'))

        if name in g['users'] and myhash((SALT + password).encode()) == g['users'][name]:
            session['user'] = name
            flash('login succeeded', 'success')
            resp = make_response(redirect(url_for('index')))
            if request.form.get('remember'):
                token = myhash((SALT + name).encode()).decode()
                if token not in g['tokens'].keys():
                    g['tokens'][token] = name
                resp.set_cookie('remember_token', value=token, max_age=60*60*24)
            return resp
        else:
            flash('login failed: {}'.format(name), 'danger')

    return redirect(url_for('index'))

@app.route('/logout')
def logout():
    session.pop('user', None)
    resp = make_response(redirect(url_for('index')))
    resp.set_cookie('remember_token', value='', expires=0)
    return resp


if __name__ == '__main__':
    app.run(host="0.0.0.0", port="11000")
