#include "zip.h"

int zip_check_header(FILE *fp,
		     EndOfCentralDirectoryRecord *footer,
		     char *comment)
{
  short comment_length;
  unsigned short word;
  unsigned int filesize;

  // Check for comment
  fseek(fp, 0L, SEEK_END);
  filesize = ftell(fp);
  fseek(fp, -2, SEEK_END);
  fread(&comment_length, 2, 1, fp);
  if (comment_length == 0) {
    comment[0] = '\x00';
  } else {
    comment_length = 1;
    while(1) {
      fseek(fp, -comment_length, SEEK_END);
      fread(&word, 2, 1, fp);
      if (word == comment_length - 2) {
	comment_length -= 2;
	break;
      }
      comment_length++;
      if (comment_length >= filesize) {
	break;
      }
    }
    if (comment_length < filesize) {
      fseek(fp, -comment_length, SEEK_END);
      fread(comment, 1, comment_length, fp);
      comment[comment_length] = '\x00';
    } else {
      comment[0] = '\x00';
    }
  }
  
  fseek(fp, -sizeof(EndOfCentralDirectoryRecord) - comment_length, SEEK_END);
  fread(footer, sizeof(EndOfCentralDirectoryRecord), 1, fp);

  // Check signature
  if (footer->signature != MAGIC_END_OF_CENTRAL_DIRECTORY_RECORD) {
    return 1;
  }
  fseek(fp, footer->cd_offset, SEEK_SET);
  return 0;
}

int zip_get_entries(FILE *fp,
		    EndOfCentralDirectoryRecord *footer,
		    CentralDirectoryFileHeader ***entry)
{
  int i;

  // Initialize
  *entry = (CentralDirectoryFileHeader**)malloc(sizeof(CentralDirectoryFileHeader*) * footer->total_entries);
  for(i = 0; i < footer->total_entries; i++) {
    (*entry)[i] = (CentralDirectoryFileHeader*)malloc(sizeof(CentralDirectoryFileHeader) - sizeof(char*));
    (*entry)[i]->filename = (char*)malloc(64);
  }

  // Read all entries
  for(i = 0; i < footer->total_entries; i++) {
    fread((*entry)[i], sizeof(CentralDirectoryFileHeader) - sizeof(char*), 1, fp);
    // Check signature
    if ((*entry)[i]->signature != MAGIC_CENTRAL_DIRECTORY_FILE_HEADER) {
      return 1;
    }
    // Read filename
    fread((*entry)[i]->filename, 1, (*entry)[i]->length_filename, fp);
    fseek(fp, (*entry)[i]->length_extra + (*entry)[i]->length_comment, SEEK_CUR);
  }
  return 0;
}

void zip_ts2time(uint16_t timestamp, int* hour, int* minute, int* second)
{
  *second = (timestamp & 0b11111) << 1;
  *minute = (timestamp >> 5) & 0b111111;
  *hour   = timestamp >> 11;
}

void zip_ts2date(uint16_t timestamp, int* year, int* month, int* date)
{
  *date  = timestamp & 0b11111;
  *month = (timestamp >> 5) & 0b1111;
  *year  = 1980 + (timestamp >> 9);
}
