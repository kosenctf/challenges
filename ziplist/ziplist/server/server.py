#!/usr/bin/env python
# coding: utf-8
from flask import Flask, request, render_template
import base64
import commands
import os
import random
import string
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

app = Flask(__name__)
app.config['MAX_CONTENT_LENGTH'] = 1 * 1024 * 1024

@app.route('/', methods=['GET', 'POST'])
def home():
    result = ''
    if 'upload' in request.files:
        zipfile = request.files['upload']
        filename = zipfile.filename
        if filename != '':
            upload_filename = "/tmp/uploads/" + ''.join(
                [random.choice(string.ascii_letters) for i in range(16)]
            ) + ".zip"
            zipfile.save(upload_filename)
            result = commands.getoutput("timeout -sKILL 1s ./ziplist " + upload_filename)
            try:
                os.remove(upload_filename)
            except OSError:
                pass
    result = base64.b64encode(result)
    return render_template('index.html', result=result)

if __name__ == '__main__':
    app.run(debug=False, host="0.0.0.0", port=9300)
