<?php
/**
 * Secure Cryptosystem
 */
class SecureCrypto
{
    /**
       Initialize this instance
     */
    public function __construct()
    {
    }

    /**
     * Encrypt with AES 256 CBC
     */
    public function encrypt($password, $data)
    {
	$iv_size = openssl_cipher_iv_length('AES-256-CBC');

	$key = hash('sha256', $password, TRUE);
	$iv  = openssl_random_pseudo_bytes($iv_size);
	
	$cipher = openssl_encrypt($data, 'AES-256-CBC', $key, OPENSSL_RAW_DATA, $iv);
	return base64_encode($iv . $cipher);
    }

    /**
     * Decrypt with AES 256 CBC
     */
    public function decrypt($password, $data)
    {
	$iv_size = openssl_cipher_iv_length('AES-256-CBC');

	$encrypted = base64_decode($data);
	$iv = substr($encrypted, 0, $iv_size);
	$cipher = substr($encrypted, $iv_size);
	
	$key = hash('sha256', $password, TRUE);
	return openssl_decrypt($cipher, 'AES-256-CBC', $key, OPENSSL_RAW_DATA, $iv);
    }
}
