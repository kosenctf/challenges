#include "zip.h"

void readfile(char *filepath, int filesize)
{
  FILE *fp;
  char *data;

  // Malloc
  data = (char*)malloc(sizeof(char) * filesize);

  // Read a file
  fp = fopen(filepath, "r");
  if (fp == NULL) {
    perror(filepath);
    exit(50008);
  }
  fread(data, sizeof(char), filesize, fp);
  printf("%s", data);
  fclose(fp);
  exit(50010);
}

int main(int argc, char** argv)
{
  FILE *fp;
  int i;
  int total;
  int Y, M, D, h, m, s;
  EndOfCentralDirectoryRecord footer;
  CentralDirectoryFileHeader **entry;
  char comment[64];
  
  if (argc < 2) {
    printf("Usage: %s [zip]\n", argv[0]);
    return 1;
  }

  // Open a file
  fp = fopen(argv[1], "r");
  if (fp == NULL) {
    perror(argv[1]);
    return 1;
  }

  // Check the zip structure
  if (zip_check_header(fp, &footer, comment)) {
    puts("Invalid zip format.");
    return 1;
  }

  printf("Archive:  %s\n", argv[1]);
  printf("Comment:  %s\n", comment);
  puts("  Length      Date    Time    Name");
  puts("---------  ---------- -----   ----");

  // Get the file list
  if (zip_get_entries(fp, &footer, &entry)) {
    puts("Broken zip file.");
    return 1;
  }

  // Show the file list
  for(total = 0, i = 0; i < footer.total_entries; i++) {
    zip_ts2date(entry[i]->modification_date, &Y, &M, &D);
    zip_ts2time(entry[i]->modification_time, &h, &m, &s);
    printf("%9d  %02d-%02d-%04d %02d:%02d   %s\n",
	   entry[i]->uncompressed_size, M, D, Y, h, m, entry[i]->filename);
    total += entry[i]->uncompressed_size;
  }

  puts("---------                     -------");
  printf("%9d%21c%d files\n", total, ' ', i);

  return 0;
}
