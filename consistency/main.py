from Crypto.Cipher import AES
from Crypto import Random
from hashlib import md5
from secret import KEY, FLAG
import sys
import os

BS = AES.block_size

def pad(s, l):
    l = l - (len(s) % l)
    return s + chr(l) * l

def unpad(s):
    l = ord(s[-1])
    return s[:-l]

def emit_token(user):
    iv = Random.new().read(BS)
    iv_check = md5(iv).digest()

    aes = AES.new(KEY, AES.MODE_CBC, iv)
    c = aes.encrypt(iv_check + pad(user, BS))
    return iv + c

def decode_token(token):
    iv, c = token[:BS], token[BS:]

    aes = AES.new(KEY, AES.MODE_CBC, iv)
    m = aes.decrypt(c)
    iv_check, m = m[:BS], m[BS:]

    if md5(iv).digest() != iv_check:
        return False

    return unpad(m)

sys.stdout = os.fdopen(sys.stdout.fileno(), 'w', 0)
print("Welcome to encryption service!")
while True:
    print("Please [r]egister or [l]ogin")
    cmd = raw_input().strip()

    if not cmd:
        break

    if cmd == 'r':
        sys.stdout.write("Name:")
        name = raw_input().strip()

        if len(name) > 32:
            print("username too long!")
        elif not name.isalnum():
            print("use only alphanumeric characters!")
        elif pad(name, BS) == pad("admin", BS):
            print("You cannot use this name!")
        else:
            print("Here is your token:")
            print(emit_token(name).encode("hex"))
            break

    elif cmd == 'l':
        sys.stdout.write("Token:")
        token = raw_input().strip()
        try:
            token = token.decode("hex")
            name = decode_token(token)

            if name == "admin":
                print("Welcome admin!")
                print(FLAG)
                break

            else:
                print("Welcome {}!".format(name))
                break

        except Exception as e:
            print("Something wrong")
    else:
        print("Unknown cmd!")
