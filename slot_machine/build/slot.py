""" Slot Machine """
import random

class Slot(object):
    SYMBOLS = {
        '7': 5,
        '$': 3,
        '*': 2,
        'x': 2,
        '-': 1,
        '-': 1
    }

    def __init__(self):
        """ Initialize and reset this instance """
        self.reels = [
            [None for j in range(3)]
            for i in range(3)
        ]
        
    def pull_lever(self):
        """ Play a slot """
        sym_list = self.SYMBOLS.keys()
        for j in range(3):
            r = random.randrange(0, len(self.SYMBOLS))
            self.reels[0][j] = sym_list[r - 1]
            self.reels[1][j] = sym_list[r]
            self.reels[2][j] = sym_list[(r + 1) % len(self.SYMBOLS)]

    def calc_bet(self, bet):
        """ Calculates the win/loss """
        d = 0
        # Vertical
        if self.reels[1][0] == self.reels[1][1] == self.reels[1][2]:
            d += bet * self.SYMBOLS[self.reels[1][1]]
        # Slant
        if self.reels[0][0] == self.reels[1][1] == self.reels[2][2]:
            d += bet * self.SYMBOLS[self.reels[1][1]]
        if self.reels[0][2] == self.reels[1][1] == self.reels[2][0]:
            d += bet * self.SYMBOLS[self.reels[1][1]]
        return d

    def show_slot(self):
        """ Show the slot machine """
        print("      .-------.       ")
        print("   oO{- KOSEN -}Oo    ")
        print("   .=============. __ ")
        print("   | [{0[0]}] [{0[1]}] [{0[2]}] |(  )".format(self.reels[0]))
        print("   | [{0[0]}] [{0[1]}] [{0[2]}] | || ".format(self.reels[1]))
        print("   | [{0[0]}] [{0[1]}] [{0[2]}] | || ".format(self.reels[2]))
        print("   |             |_|| ")
        print("   | 777 ::::::: |--' ")
        print("   | $$$ ::::::: |    ")
        print("   | *** ::::::: |    ")
        print("   |             |    ")
        print("   |      __ === |    ")
        print("   |_____/__\____|    ")
        print("  /###############\   ")
        print(" /#################\  ")
        print("|#JGS###############| ")
