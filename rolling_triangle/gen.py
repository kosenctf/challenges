import math
import sys


def dft(xs):
    N = len(xs)
    res = []
    ims = []

    for i in range(N):
        re = 0.0

        for j in range(N):
            x = 2.0 * math.pi * i * j / N
            re += xs[j] * math.cos(-x)

        res.append(re / N)

    return res


def idft(res):
  N = len(res)
  x = []
  for i in range(N):
    re = 0.0
    for j in range(N):
      t = j * (2.0 * math.pi) * (float(i) / N)
      re += (res[j] * math.cos(t)) - (res[j] * math.sin(t))
    x.append(re)
  return x

def sampling(f, N):
    xs = []
    for i in range(N):
        xs.append( f(2.0 * math.pi * i / N) )
    return xs


def encode(s):
    N = len(s)
    rs = [0.0 for _ in range(N)]
    for i in range(N):
        a = ord(s[i])
        for j in range(N):
            rs[j] += a * math.cos( 2.0 * math.pi * i * j / N  )

    return rs

# xs = encode("KOSENCTF{DO_YOU_KNOW_OF_METAL_MOMOKO}")
# res = dft(xs)

# print(", ".join(["{:.5f}".format(r) for r in res]))
# print(", ".join(["{:.5f}".format(r) for r in ims]))

# print(len(xs))
# print(", ".join(["{:.5f}".format(r) for r in res]))
print(len(sys.argv[1]))
ys = idft(list(map(ord, sys.argv[1])))
print(", ".join(["{:.5f}".format(r) for r in ys]))

