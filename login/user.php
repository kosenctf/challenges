<?php

if (! isset($_SESSION['login'])) {
        die('<a href="index.php">PLEASE LOGIN PLEASE</a>');
}

if ($_SESSION['login'] === 'admin') {
        $flag = include(__DIR__ . '/flag.php');
       echo "<h3>HELLO ADMIN!</h3>"; 
       echo "<h3>$flag</h3>"; 
} else {
       echo "<h4>HELLO " . htmlspecialchars($_SESSION['login']) ."</h4>"; 
       echo "<p>Nice login, but you are not an admin!</p>";
       echo "<p><a href='logout.php'>Logout</a></p>";
}

