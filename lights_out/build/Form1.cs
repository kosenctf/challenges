﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LightsOut
{
    public partial class MainForm : Form
    {
        private bool[,] map = new bool[20, 20];

        public MainForm()
        {
            InitializeComponent();
            Initialize();
        }

        public void Initialize()
        {
            // Randomize
            Random rnd = new System.Random();

            // Create Buttons
            for (int i = 0; i < 20; i++)
            {
                for (int j = 0; j < 20; j++)
                {
                    Button button = new Button();
                    button.BackColor = System.Drawing.Color.Black;
                    button.Location = new System.Drawing.Point(32 * j, 32 * i);
                    button.Name = String.Format("btn{0:D2}{1:D2}", i, j);
                    button.Size = new System.Drawing.Size(32, 32);
                    button.TabIndex = 0;
                    button.UseVisualStyleBackColor = false;
                    button.Click += new EventHandler(ButtonClick);
                    this.Controls.Add(button);
                    // initialize the map
                    map[i, j] = false;
                    if (rnd.Next(2) == 0) this.Reverse(j, i);
                }
            }
            this.Size = new System.Drawing.Size(32 * 20 + 16, 32 * 20 + 40);
            
            this.Reverse(0, 0);
        }

        private void ButtonClick(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            int x = int.Parse(button.Name.Substring(5, 2));
            int y = int.Parse(button.Name.Substring(3, 2));
            for (int i = -1; i <= 1; i++) {
                for (int j = -1; j <= 1; j++) {
                    if (x + j >= 0 && x + j < 20 && y + i >= 0 && y + i < 20) {
                        this.Reverse(x + j, y + i);
                    }
                }
            }

            Check();
        }

        private void Check()
        {
            for (int i = 0; i < 20; i++) {
                for (int j = 0; j < 20; j++) {
                    if (!map[i, j]) return;
                }
            }
            MessageBox.Show("KOSENCTF{st4tic4lly_d3obfusc4t3_OR_dyn4mic4lly_ch34t}", "Congratulations!", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void Reverse(int x, int y)
        {
            String name = String.Format("btn{0:D2}{1:D2}", y, x);
            Button button = (Button)Controls.Find(name, true)[0];
            map[y, x] ^= true;
            if (map[y, x]) {
                button.BackColor = System.Drawing.Color.Yellow;
            } else {
                button.BackColor = System.Drawing.Color.Black;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }
    }
}
