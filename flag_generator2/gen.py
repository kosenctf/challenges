import random
import sys


arr = []
dp = {}

N = 1000

def f(n):
    if n == 0:
        return arr[0]
    if n == 1:
        return arr[1]
    else:
        if n in dp:
            return dp[n]
        else:
            r = arr[n] ^ f(n-1) ^ f(n-2)
            dp[n] = r
            return r


for i in range(N):
    arr.append(random.randint(0, 0xFF))

for i, c in enumerate(sys.argv[1]):
    x = f(N + i - 1) ^ f(N + i - 2) ^ ord(c)
    arr.append(x)



script = "(eval '(progn "
script += ("(setq ls (make-vector {} 0))".format(len(arr)))
for i, a in enumerate(arr):
    script += ("(aset ls {} {})".format(i, a))


script += ("""
(defun f (x)
    (if (<= x 1)
        (aref ls x)
        (logxor (aref ls x) (f (- x 1 )) (f (- x 2)))))
""")

for i in range(len(sys.argv[1])):
   script += ("(insert (f {}))".format(N + i))
script+="))"

print("(setq a (vector {}))".format(" ".join([str(ord(c) ^ i) for i, c in enumerate(script)])))
print('(setq b "")')
print('(dotimes (i {}) (setq b (concat b (byte-to-string (logxor i (aref a i))))))'.format(len(script)))
print('(eval (read b))')
