from Crypto.Util import number
from Crypto.PublicKey import RSA
import gmpy
import sys

def root_e(c, e):
    m = gmpy.root(c, e)[0]
    return long(m)

def ashexstr(h):
    s = "{:0x}".format(h)
    if len(s) % 2 == 1:
        s = "0" + s
    return s.decode("hex")

x, y, z = open(sys.argv[1]).read().split()
c = int(x.split("=")[1])
e = 3
n = int(z.split("=")[1])

# print(ashexstr(root_e(c, e)))

c2 = c
for i in range(10):
    c2 += n
    s = bin(root_e(c2, e))
    if s.endswith("00000000000"):
        print(ashexstr(int(s.rstrip("0")[2:], 2)))
        break

