<?php
/**
 * A Sample Script For Secure Session Manager
 */
require_once(__DIR__ . '/modules/crypto.php');
require_once(__DIR__ . '/modules/session.php');
ini_set('display_errors', 1);
ini_set('session.use_strict_mode', 1);
ini_set('session.name', "KOSESSID");

/* Initialize */
if (isset($_POST['save'])) {
    // You can skip the bothering setup
    // by loading the saved handler.
    $handler = unserialize(base64_decode($_POST['save']));
} else {
    // Or, you can also setup the handler manually.
    $SECRET_KEY = 'sample-password'; // Keep it secret!
    $crypto = new SecureCrypto();
    $handler = new SecureSession($SECRET_KEY);
    $handler->set_crypto('encrypt', array($crypto, 'encrypt'));
    $handler->set_crypto('decrypt', array($crypto, 'decrypt'));
}
session_set_save_handler($handler, true);

/* Now you can start the secure session */
session_start();

/* Use the session as usual */
if (isset($_SESSION['count'])) {
    $_SESSION['count'] += 1;
} else {
    $_SESSION['count'] = 1;
}
?>

<!DOCTYPE html>
<html>

    <head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
	<title>Secure Session Manager</title>
    </head>

    <body>
	<div class="jumbotron">
            <div class="container">
		<h1 class="display-3">Secure Session Manager</h1>
		<p>
		    SSM is a fully-php-made module which keeps all the sessions encrypted.
		    It takes just few steps to setup SSM, and your website will be protected with AES.
		    Try this demo and be surprised at its security!
		</p>
            </div>
	</div>
	<div class="container">
	    <div class="row">
		<div class="col-md-4">
		    <h2>Demo</h2>
		    <p>
			This is a simple access counter.<br>
		    </p>
		    <code>Access Count: <?= $_SESSION['count']; ?></code>
		</div>
		<div class="col-md-4">
		    <h2>Encrypted?</h2>
		    <p>
			This is the raw data of the PHP session:
		    </p>
		    <code style="overflow-wrap: break-word;">
			<?php print_r($handler->read_raw(session_id())); ?>
		    </code>
		    <p>
			This data would be a plaintext if it were not for SSM!
		    </p>
		</div>
		<div class="col-md-4">
		    <h2>Simple?</h2>
		    <p>You can easily setup SSM by using saved handler.</p>
		    <form method="POST">
			<input type="hidden" name="save" value="<?= base64_encode(serialize($handler)); ?>">
			<button class="btn btn-primary">Use Saved Handler</button>
		    </form><br>
		    <p>See the <a href="/ssm.tar.gz">source code</a> for the detail.</p>
		</div>
	    </div>
	</div>
    </body>

</html>
