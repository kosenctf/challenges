import functools
import binascii


def myhash(message):
    xs = myhash_impl(message)
    return binascii.hexlify(functools.reduce(lambda x, y: x + y, [x.to_bytes(4, 'big') for x in xs]))

def myhash_impl(message, iv=(0xDEADBEEF, 0xCAFEBABE, 0x00C0FFEE, 0x5017B047)):
    # add padding
    l = (64 - len(message)) % 64
    message += l.to_bytes(1, 'big') * l

    A, B, C, D = iv

    f = lambda x, y: (((x<<1)^(y>>5)) ^ ((x>>16)|(y<<16))) & 0xFFFFFFFF
    g = lambda x, y: ((x<<4) | ((y << 1) ^ (y >> 10))) & 0xFFFFFFFF
    h = lambda x, y: (x|y) ^ (x&y)

    for i in range(0, len(message), 16):
        a = int.from_bytes(message[i:i+4], 'big')
        b = int.from_bytes(message[i+4:i+8], 'big')
        c = int.from_bytes(message[i+8:i+12], 'big')
        d = int.from_bytes(message[i+12:i+16], 'big')

        A = f(g(B, a), h(C, d))
        B = f(g(C, b), h(D, a))
        C = f(g(D, c), h(A, b))
        D = f(g(A, d), h(B, c))

    return [A, B, C, D]


