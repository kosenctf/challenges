#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

void handler(int sig)
{
  if (sig == SIGALRM) {
    puts("\nNo need to answer my question. Bye.");
    exit(1);
  }
}

void init(void)
{
  setvbuf(stdout, NULL, _IONBF, 0);
  signal(SIGALRM, handler);
  alarm(5);
}

int main(void)
{
  char buffer[128];
 
  init();
  puts("May I ask your name?");
  printf("First Name: ");
  scanf("%127s", buffer);
  printf(buffer);
  printf("\nFamily Name: ");
  scanf("%127s", buffer);
  printf(buffer);
  puts("\nThanks.");
  return 0;
}
