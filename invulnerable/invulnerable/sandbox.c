/*
 * (1) ASLR - Makes it difficult to guess the libc/stack addresses
 * (2) RELRO - Protects you against GOT overwrite
 * (3) SSP - Detects stack overflow
 * (4) NX - Disables RW data execution
 * (5) PIE - Makes it difficult to specify the function addresses
 * (6) seccomp - Prevents from issuing any unintended syscalls
 */
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <string.h>
#include <seccomp.h>
#include <signal.h>

#define SIZE_STACK 64
#define SIZE_CODE  64

char *stack;
char *code;

void handler(int sig)
{
  if (sig == SIGALRM) {
    exit(0);
  }
}

void sb_init(void)
{
  scmp_filter_ctx ctx;
  signal(SIGALRM, handler);
  alarm(5);
  ctx = seccomp_init(SCMP_ACT_KILL);
  if (!ctx) {
    seccomp_reset(ctx, 0LL);
    exit(1);
  }
  seccomp_arch_add(ctx, SCMP_ARCH_X86_64);
  seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(read), 0);
  seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(open), 0);
  seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(close), 0);
  seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 0);
  seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mprotect), 0);
  seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(munmap), 0);
  seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(brk), 0);
  seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(exit), 0);
  seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(exit_group), 0);
  seccomp_load(ctx);
}

void sb_read()
{
  stack = (char*)mmap(NULL,
		      SIZE_STACK,
		      PROT_READ | PROT_WRITE,
		      MAP_PRIVATE | MAP_ANONYMOUS,
		      -1, 0);
  memset(stack, 0, SIZE_STACK);
  code = (char*)mmap(NULL,
		     SIZE_CODE,
		     PROT_READ | PROT_WRITE | PROT_EXEC,
		     MAP_PRIVATE | MAP_ANONYMOUS,
		     -1, 0);
  memset(code, 0x90, SIZE_CODE);
  read(0, code + 10, SIZE_CODE - 25);
}

void sb_exec()
{
  unsigned long stack_bottom = (unsigned long)stack + SIZE_STACK;
  unsigned long addr_exit = (unsigned long)exit;
  memcpy(code, "\x48\xbc", 2);
  memcpy(code + 2, (void*)&stack_bottom, 8);
  memcpy(code + SIZE_CODE - 15, "\x48\x31\xc0", 3);
  memcpy(code + SIZE_CODE - 12, "\x48\xba", 2);
  memcpy(code + SIZE_CODE - 10, (void*)&addr_exit, 8);
  memcpy(code + SIZE_CODE - 2, "\xff\xd2", 2);
  ((void(*)())code)();
}

int main()
{
  sb_init();
  sb_read();
  sb_exec();
  return 0;
}
