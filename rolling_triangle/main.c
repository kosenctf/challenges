#include <math.h>
#include <string.h>
#include <stdio.h>

#define M_PI 3.14159265358979323846 
#define N 37

double table[] = {3033.00000, -54.25961, 66.87633, 94.76591, 64.99721, 120.54047, 45.72783, 3.35002, 1.63227, 134.09445, 46.73537, -29.95092, -64.80962, 73.44755, 68.97389, -104.15891, -105.11247, 42.73484, 60.69246, 4.60207, -138.58525, -132.18342, -55.53481, -63.53289, 18.77198, -205.92473, -104.99775, 59.76358, -89.23073, -25.15440, -93.54309, 26.30905, 45.91751, 59.00803, -37.37488, -26.28738, 33.70007};


void idft(char *is, double *os)
{
  int i, j;
  double t;
  for (i = 0; i < N; i++) {
    os[i] = 0.0;
    for (j = 0; j < N; j++) {
      t = j * (2.0 * M_PI * i) / N;
      os[i] += is[j] * cos(t) - is[j] * sin(t);
    }
  }
}

int is_equal(double a, double b) {
  if (fabs(a - b) < 0.001) {
    return 1;
  }
  return 0;
}

int main(int argc, char **argv) {
  double os[N] = {0.0};
  int i;

  if (argc == 1) {
    printf("Usage: %s flag\n", argv[0]);
    return 0;
  }

  if (strlen(argv[1]) != N) {
    printf("Wrong...\n");
    return 0;
  }

  idft(argv[1], os);

  for (i = 0; i < N; i++) {
    if (is_equal(table[i], os[i]) == 0) {
      printf("Wrong...\n");
      return 0;
    }
  }
  printf("Correct!\n");

  return 0;
}
