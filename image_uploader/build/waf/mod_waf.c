/*
  # apxs -i -a -c mod_waf.c
  # cp /lib64/httpd/modules/mod_waf.so ./
  # strip --strip-all mod_waf.so
 */
#include "httpd.h"
#include "http_config.h"
#include "http_protocol.h"
#include "apr_strings.h"
#include "ap_config.h"
#include "util_script.h"
#include <stdio.h>

/* WAFのハンドラ */
static int waf_handler(request_rec *r)
{
  int is_admin;
  const char *filename;
  const char *password;
  long size;
  char pwd[64];
  apr_table_t *GET;
  FILE *fp;
  
  // リクエストされたファイル名を取得
  filename = apr_pstrdup(r->pool, r->filename);
  
  if (strstr(filename, "admin")) {
    is_admin = 0;
    // passwordを取得
    ap_args_to_table(r, &GET);
    password = apr_table_get(GET, "password");
    // パスワードを取得
    fp = fopen("/var/www/secret-pwd-file", "rb");
    fseek(fp, 0L, SEEK_END);
    size = ftell(fp);
    fseek(fp, 0L, SEEK_SET);
    fread(pwd, 1, size, fp);
    pwd[size] = 0;
    fclose(fp);
    if (password && strcmp(password, pwd) == 0) {
      is_admin = 1;
    }
  } else {
    return DECLINED;
  }

  if (is_admin == 0) {
    if (!r->header_only) {
      ap_rputs("<!-- mod_waf v0.1 -->\n", r);
      ap_rputs("<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML 2.0//EN\">\n", r);
      ap_rputs("<html><head>\n", r);
      ap_rputs("<title>Web Application Firewall</title>\n", r);
      ap_rputs("</head><body>\n", r);
      ap_rputs("<h1>Web Application Firewall</h1>\n", r);
      ap_rputs("<p>You are not authorized.</p>\n", r);
      ap_rputs("</body></html>\n", r);
    }
    return OK;
  }
  
  return DECLINED;
}

static void waf_register_hooks(apr_pool_t *p)
{
  ap_hook_handler(waf_handler, NULL, NULL, APR_HOOK_MIDDLE);
}

module AP_MODULE_DECLARE_DATA waf_module =
  {
   STANDARD20_MODULE_STUFF, 
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   waf_register_hooks
  };
