#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void h(char *s, char **r) {
  int l = strlen(s);
  int p = 4 - (l % 4);
  char *buf = malloc(sizeof(char) * (l + p));
  *r = malloc(sizeof(char) * (l + p));

  strncpy(buf, s, l);
  for (int i = l; i < l+p; i++) {
    buf[i] = p;
  }

  unsigned int k = 0xDEC0C0DE;

  unsigned  int hash = 0;
  for (int j = (l+p)/4 - 1; j >= 0; j--) {
    // printf("%x = %x ^ %x ^ %x\n", hash ^ ((unsigned int*)buf)[j] ^ k, hash, ((unsigned int*)buf)[j], k);
    hash ^= ((unsigned int*)buf)[j] ^ k;
    k = (k << 8) | (k >> 24);
    ((unsigned int*)(*r))[j] = hash;
  }

  free(buf);
}

int check(char *s) {
  char *buf;
  // char *x = "\xf6\xc7\xdf\xe6\x63\x48\x4c\x7d\xed\xcb\xc6\xe5\x56\x46\x57\x7a\xcd\xd9\xc4\xe3\x4c\x4d\x4b\x62\xc8\xc8\xd6\xf3\x4c\x53\x57\x67\xda\xc4\xc4\xda";
  char *x = "\x9f\xc9\xd7\xc2\x0a\x46\x44\x59\x84\xc5\xce\xc1\x3f\x4f\x5f\x4e\xbe\xd4\xde\xdd\x39\x4b\x4a\x4c\xa6\xcf\xd1\xd1\x29\x55\x4a\x4e\xa3\xc3\xc3\xdd";
  h(s, &buf);
  int r = 1;

  // printf("%s", buf);
  for (int i = 0; i < strlen(buf); i++) {
    if (buf[i] != x[i]) {
      r = 0;
      break;
    } 
  }

  free(buf);
  return r;
}

int main(int argc, char **argv) {
  if (argc == 1) {
    printf("No flag is provided.\n");
    return 1;
  }
  if (check(argv[1]) == 1) {
    printf("This is a flag.\n");
  } else {
    printf("This is not a flag.\n");
  }

  return 0;
}
