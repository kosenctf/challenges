import sys
import requests

def pad(message):
    l = (64 - len(message)) % 64
    message += l.to_bytes(1, 'big') * l
    return message

def work(message, l):
    # calculate padding character
    pc = pad(('x'*l + message).encode()).decode()[-1]
    l2 = (64 - (len(message) + l)) % 64
    return message + pc * l2


BASE = "admin"
URL = "http://localhost:11000/"
for s in range(64):
    name = work(BASE, s)
    r = requests.post(URL + "register", data={"name": name, "password": "hoge"})
    r = requests.post(URL + "login", data={"name": name, "password": "hoge", "remember": "on"}, allow_redirects=False)


    c = {
            "remember_token": r.cookies.get_dict()['remember_token']
    }

    r = requests.get(URL, cookies=c)
    if "Save" in r.text:
        print(r.text)
        print(repr(name))
        break


