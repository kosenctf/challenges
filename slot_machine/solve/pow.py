import hashlib
import string
import random
import sys

target = sys.argv[1]

while True:
    text = ''.join([random.choice(string.ascii_letters) for i in range(16)])
    md5 = hashlib.sha256(text).hexdigest()
    if md5[-5:] == target[-5:]:
        print(text)
        print(md5, target)
        break
