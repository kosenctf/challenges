<?php
/**
 * Secure Session Manager
 */
class SecureSession extends SessionHandler
{
    private $crypto;
    private $key;
    
    /**
     * Initialize this instance
     */
    public function __construct($key)
    {
	// Set variables
	$this->key = $key;
	$this->crypto = array();
    }

    /**
     * Set a cryptosystem
     */
    public function set_crypto($crypto, $function)
    {
	$this->crypto[$crypto] = $function;
    }

    /**
     * Read and decrypt session data
     */
    public function read($id)
    {
	$data = parent::read($id);

	if (!$data) {
	    return "";
	} else {
	    return $this->crypto['decrypt']($this->key, $data);
	}
    }

    /**
     * Encrypt and write session data
     */
    public function write($id, $data)
    {
	$data = $this->crypto['encrypt']($this->key, $data);
	
	return parent::write($id, $data);
    }

    /**
     * Read the raw (encrypted) session data
     */
    public function read_raw($id)
    {
	parent::open(session_save_path(), $id);
	$data = parent::read($id);
	return $data;
    }
}
?>
