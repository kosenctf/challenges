import base64
from fastecdsa.asn1 import _asn1_public_key
from fastecdsa.curve import P256
from fastecdsa.point import Point
from secret import a, b, c, prime

def sign(m):
    """ Calculate a signature for the money """
    G, q = P256.G, P256.q
    if m >= q:
        raise ValueError("Too large value to sign.")
    if m <= -q:
        raise ValueError("Too small value to sign.")
    sign = 1 if m >= 0 else -1
    P, Q, R = sign*a*G, sign*b*G, c*G
    S = pow(m, prime, q)*G + m*m*P + m*Q + R
    enc = _asn1_public_key(S)
    return base64.b64encode(enc)

def save_token(money):
    """ Save a signed token """
    return str(money) + '@' + sign(money)

def load_token(token):
    """ Load a signed token """
    try:
        money, signature = token.split('@')
        if money < 0:
            raise ValueError("Something is wrong.")
        if sign(int(money)) == signature:
            return int(money)
    except:
        pass
    return None
