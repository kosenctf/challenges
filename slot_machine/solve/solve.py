import base64
from fastecdsa.asn1 import _asn1_public_key
from fastecdsa.asn1 import decode_key
from fastecdsa.curve import P256
from fastecdsa.point import Point

def egcd(a, b):
    if a == 0:
        return (b, 0, 1)
    else:
        g, y, x = egcd(b % a, a)
        return (g, x - (b // a) * y, y)

def modinv(a, m):
    g, x, y = egcd(a, m)
    if g != 1:
        raise Exception('modular inverse does not exist')
    else:
        return x % m

def token2point(token):
    from fastecdsa.asn1 import ASN1_PARSED_DATA, OBJECT_IDENTIFIER
    _, token = token.split('@')
    ASN1_PARSED_DATA.append((OBJECT_IDENTIFIER, P256.oid))
    return decode_key(base64.b64decode(token))[1]

G, q = P256.G, P256.q
Sn1 = token2point('-1@A0IABCOzUtB+jjcg7rSy0h2xBOf2DrrTJlW1n239LGrCMi+KwVGcQXCMM/UfD5GQA8XKpGpL0DCob/A6wEcrmNoq6Xg=')
S0  = token2point('0@A0IABGA062NzBN0dFxWwXv+xzMgfbc6A3048RVTrDEMajEHizL/r/szmYLR1PkLVwfCzb8HGAElwvK1k9jUFmqcX4Rg=')
S1  = token2point('1@A0IABHZo6IoUfkynj8h19v+JT07Kj+vUIrD6YFOoKIzUiF/D7lc6pn51ux65oXs2jrIpTl40suHgjjolCJHH2bU+28c=')

R = S0
Q = modinv(2, q) * (Sn1 + S1) - R
P = S1 - G - Q - R
m = q - 1
S = -G + P - Q + R

print(R)
print(P)
print(Q)

enc = _asn1_public_key(S)
print(str(m) + "@" + base64.b64encode(enc))
