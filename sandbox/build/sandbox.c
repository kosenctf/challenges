/*
 * (1) ASLR - Makes it difficult to guess the libc/stack addresses
 * (2) RELRO - Protects you against GOT overwrite
 * (3) SSP - Detects stack overflow
 * (4) NX - Disables RW data execution
 * (5) PIE - Makes it difficult to specify the function addresses
 * (6) seccomp - Prevents from issuing any unintended syscalls
 */
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <sys/mman.h>
#include <seccomp.h>

#define SIZE_STACK 0x1000
#define SIZE_CODE  0x1000

typedef struct {
  unsigned int r1;
  unsigned int r2;
  unsigned int pc;
  unsigned int sp;
  unsigned char zf;
} reg_t;
unsigned char *stack;
unsigned char *code;
reg_t *reg;

void handler(int sig)
{
  if (sig == SIGALRM) {
    exit(0);
  }
}

void sb_init(void)
{
  scmp_filter_ctx ctx;
  signal(SIGALRM, handler);
  alarm(5);
  ctx = seccomp_init(SCMP_ACT_KILL);
  if (!ctx) {
    seccomp_reset(ctx, 0LL);
    exit(1);
  }
  seccomp_arch_add(ctx, SCMP_ARCH_X86_64);
  seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(read), 0);
  seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(open), 0);
  seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(openat), 0);
  seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(close), 0);
  seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 0);
  seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mprotect), 0);
  seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(munmap), 0);
  seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(brk), 0);
  seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(exit), 0);
  seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(exit_group), 0);
  seccomp_load(ctx);
}

void sb_read()
{
  stack = (unsigned char*)malloc(sizeof(unsigned char) * SIZE_STACK);
  memset(stack, 0, SIZE_STACK);
  code = (unsigned char*)malloc(sizeof(unsigned char) * SIZE_CODE);
  memset(code, 0x90, SIZE_CODE);
  reg = (reg_t*)malloc(sizeof(reg_t));
  memset(reg, 0, sizeof(reg_t));
  read(0, code, SIZE_CODE);
}

void sb_syscall(unsigned short val)
{
  switch(val) {
  case 0x0: // exit()
    reg->pc = SIZE_CODE;
    break;
  case 0x1: // read(r1, sp, r2)
    read(reg->r1, &stack[reg->sp], reg->r2);
    break;
  case 0x2: // open(r1, O_RDONLY);
    reg->r1 = open(&stack[reg->r1], O_RDONLY);
    break;
  case 0x3: // close(r1);
    close(reg->r1);
    break;
  default:
    break;
  }
}

void sb_exec_instruction(unsigned char ope, unsigned short val)
{
  switch(ope) {
  case 0b0000: // push(r1)
    memcpy(&stack[reg->sp], &reg->r1, sizeof(reg->r1));
    reg->sp += sizeof(reg->r1);
    break;
  case 0b0001: // push(r2)
    memcpy(&stack[reg->sp], &reg->r2, sizeof(reg->r2));
    reg->sp += sizeof(reg->r2);
    break;
  case 0b0010: // pop(r1)
    memcpy(&reg->r1, &stack[reg->sp], sizeof(reg->r1));
    reg->sp -= sizeof(reg->r1);
    break;
  case 0b0011: // pop(r2)
    memcpy(&reg->r2, &stack[reg->sp], sizeof(reg->r2));
    reg->sp -= sizeof(reg->r2);
    break;
  case 0b0100: // mov r1, val
    reg->r1 = val;
    break;
  case 0b0101: // mov r2, val
    reg->r2 = val;
    break;
  case 0b0110: // add r1, val
    reg->r1 += val;
    break;
  case 0b0111: // sub r1, val
    reg->r1 -= val;
    break;
  case 0b1000: // mov r1, sp
    reg->r1 = reg->sp;
    break;
  case 0b1001: // mov r1, byte [sp + val]
    reg->r1 = stack[reg->sp + val];
    break;
  case 0b1010: // cmp r1, r2
    if (reg->r1 == reg->r2) {
      reg->zf = 1;
    } else {
      reg->zf = 0;
    }
    break;
  case 0b1011: // jz val
    if (reg->zf == 1) {
      reg->pc = val - 2;
    }
    break;
  case 0b1100: // jnz val
    if (reg->zf == 0) {
      reg->pc = val - 2;
    }
    break;
  case 0b1101: // jmp val
    reg->pc = val - 2;
    break;
  case 0b1110: // shl r1, val
    reg->r1 <<= val;
    break;
  case 0b1111: // syscall
    sb_syscall(val);
    break;
  }
}

void sb_exec()
{
  for(reg->pc = 0; reg->pc < SIZE_CODE; reg->pc += 2) {
    unsigned char ope = code[reg->pc] >> 4;
    unsigned short val = ((code[reg->pc] & 0xF) << 8) | code[reg->pc + 1];
    sb_exec_instruction(ope, val);
  }
}

int main()
{
  sb_init();
  sb_read();
  sb_exec();
  return 0;
}
