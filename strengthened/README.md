# strengthened [crypto 100]

RSA 

## 問題文

```
If you choose a tiny `e` as an RSA public exponent key, it may have the **Low Public Exponent Attack** vulnerability.
So, I strengthened it.
```
