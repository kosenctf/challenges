collect:
	mkdir static
	find . -name "*.tar.gz" -exec cp {} static \;

clean:
	rm -rf static
