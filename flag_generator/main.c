#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>


int s;

int r() {
  s = ((s * 1103515245U) + 12345U) & 0x7fffffff;
  return s;
}

int main() {
  int xs[] = {0x608f5935, 0x57506491, 0x27365557, 0x54e3dea1, 0x755a4ed5, 0x17f42eb7, 0x4a4f9059, 0x1a08e827, 0x0d9d391f, 0x59e533aa};
  int first = 0x25dc167e;

  int xlen = 10;
  int cnt = 0;
  int flag = 0;
  int v = 0;
  while (1) {
    if (!flag) {
      s = time(NULL);
    }

    v = r();
    if (v == first) {
      flag = 1;
    }
    if (flag) {
      v ^= xs[cnt];
      printf("%s", (char*)&v);
      cnt++;
      if (cnt == xlen) {
        break;
      }
    }

    sleep(1);
  }

  return 0;
}
